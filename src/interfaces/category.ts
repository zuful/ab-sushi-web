import { Piece } from './piece';

export interface Category {
    id: string;
    name: string;
    pieces: Array<Piece>;
}
