export interface PiecesOrder {
    pieceId: string;
    quantity: number;
}
