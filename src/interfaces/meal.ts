export interface Meal {
    id: string;
    userId: string;
    restaurantId: string;
    date: string;
    active: boolean;
    number: number;
    mailindDate: string;
}
