import { Category } from './category';

export interface Restaurant {
    id: string;
    name: string;
    menu: Array<Category>;
}
