import { PiecesOrder } from './pieces-order';

export interface Order {
    id: string;
    userId: string;
    piecesOrder: Array<PiecesOrder>;
}
