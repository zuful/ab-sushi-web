import { Component, OnInit } from '@angular/core';
import {Category} from '../../interfaces/category';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Restaurant} from '../../interfaces/restaurant';
import {ActivatedRoute, Router} from '@angular/router';
import {Meal} from '../../interfaces/meal';
import {FormControl} from '@angular/forms';
import {Order} from '../../interfaces/order';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {

  public status = '';
  public formCtrl = new FormControl('');
  private baseServiceUrl = 'http://localhost:8080';
  public restaurant: Restaurant;
  public mealId: string;
  public order: Order = {} as Order;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {

    this.order.userId = localStorage.getItem('userId');

    this.status = 'pending';
    const mealId = route.snapshot.params['mealId'];
    const urlMeal = this.baseServiceUrl + '/meal/' + mealId;

    this.http.get(urlMeal, {headers: new HttpHeaders().set('content-type' , 'application/json')}).subscribe( resMeal => {

      const meal: Meal = resMeal as Meal;
      this.mealId = meal.id;
      const restaurantId = meal.restaurantId;
      const urlRestaurant = this.baseServiceUrl + '/restaurant/' + restaurantId;

      this.http.get(urlRestaurant, {headers: new HttpHeaders().set('content-type' , 'application/json')}).subscribe(resRestaurant => {

        this.restaurant = resRestaurant as Restaurant;
        this.status = 'success';

      }, errRestaurant => {

        this.status = 'failure';
        console.log(errRestaurant);

      });

    }, errMeal => {

      this.status = 'failure';
      console.log(errMeal);

    });
  }

  ngOnInit() {}

  createOrder(order: Order) {

    const url = this.baseServiceUrl + '/order/' + this.mealId;

    this.http.post(url, order, {headers: new HttpHeaders().set('content-type', 'application/json' )}).subscribe((orderId) => {
      this.router.navigateByUrl('lobby/' + this.mealId );
    }, err => {
      console.log();
    });

  }
}
