import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Restaurant } from '../../interfaces/restaurant';
import { Meal } from '../../interfaces/meal';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-meal-form',
  templateUrl: './meal-form.component.html',
  styleUrls: ['./meal-form.component.css']
})
export class MealFormComponent implements OnInit {

  private baseServiceUrl: string = 'http://localhost:8080';
  public status: string = '';
  public formCtrl = new FormControl('');
  public restaurantsList: Array<Restaurant> = [] as Array<Restaurant>;

  constructor(private http: HttpClient, private router: Router, private route: ActivatedRoute) {

    this.status = 'pending';
    const url: string = this.baseServiceUrl + '/all-restaurants';

    this.http.get(url).subscribe((allRestaurants) => {
      this.restaurantsList = allRestaurants as Array<Restaurant>;
      this.status = 'success';
    });

  }

  ngOnInit() {}

  createMeal(restaurantId: string){

    const meal: Meal = {} as Meal;
    meal.restaurantId = restaurantId;
    meal.userId = 'a8102b31-fa67-4be0-b3fa-79e4eeafa70b';
    meal.date =  new Date(Date.now()).toISOString();
    meal.mailindDate =  new Date(Date.now()).toISOString();
    meal.number = 0;
    meal.active = true;

    const url = this.baseServiceUrl + '/meal';

    this.http.post(url, meal, {headers: new HttpHeaders().set('content-type', 'application/json' )}).subscribe((mealId) => {
      this.router.navigateByUrl('new-order/' + mealId);
    });

  }
}
